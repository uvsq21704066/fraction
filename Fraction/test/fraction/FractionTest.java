package fraction;

import static org.junit.Assert.*;
import java.io.IOException;
import org.junit.Test;

public class FractionTest {
	
	@Test
	public void testConsultation() throws IOException,DivisionParZeroException
	{
		Fraction f = new Fraction(1,2);
		Fraction f1 = new Fraction(3);
		Fraction f2 = new Fraction();
	
		assertEquals(1,f.getnum());
		assertEquals(2,f.getdenom());
		assertEquals(3,f1.getnum());
		assertEquals(1,f1.getdenom());
		assertEquals(0,f2.getnum());
		assertEquals(1,f2.getdenom());
	}
	
	@Test
	public void testConsultationFlottante() throws IOException,DivisionParZeroException
	{
		Fraction f = new Fraction(1,2);
		Fraction f1 = new Fraction(3);
		Fraction f2 = new Fraction();
	
		assertEquals(0.5,f.floattant(),0);
		assertEquals(3.0,f1.floattant(),0);
		assertEquals(0.0,f2.floattant(),0);
	}
	
	@Test
	public void testConversion() throws IOException,DivisionParZeroException
	{
		Fraction f = new Fraction(1,2);
		Fraction f1 = new Fraction(3);
		Fraction f2 = new Fraction();
		
		assertEquals("1/2",f.conversion());
		assertEquals("3/1",f1.conversion());
		assertEquals("0/1",f2.conversion());
	}


}
