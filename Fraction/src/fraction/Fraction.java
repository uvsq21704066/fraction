package fraction;

public final class Fraction {
	
	private final int num;
	private final int denom;
	
	public Fraction(int num,int denom) throws DivisionParZeroException
	{
		if(denom == 0) throw new DivisionParZeroException();
		
		this.num = num;
		this.denom = denom;
	}
	
	public Fraction(int num)
	{
		this.num = num;
		this.denom = 1;
	}
	
	public Fraction()
	{
		num = 0;
		denom = 1;
	}
	
	public int getnum()
	{
		return num;
	}
	
	public int getdenom()
	{
		return denom;
	}
	
	public double floattant()
	{		
		double nume = (double)num;
		double denomi = (double)denom;
		double resultat = nume/denomi;
		return resultat;
		
	}
	
	public String conversion()
	{
		return num + "/" + denom;
		
	}
	
}
